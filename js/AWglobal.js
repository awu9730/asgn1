$(document).ready(function(){
    $("#checkToggleCreate").click(function(){
        $("#section").toggle();
    });
});

$(document).ready(function(){
    $("#checkToggleEdit").click(function(){
        $("#section1").toggle();
    });
});

function Save_click(){
    checkFormAdd();
}

function Update_Click(){
    checkFormUpdate();
}

function Delete_Click(){
    deleteRecord();
}

function AWEdit_show(){
    continentList();
    showRecord();
}

function AWShowAll_show(){
    showRecordAll();
}

function DataClear(){
    clearDataBase();
}


function showList(){
    showContinent();
}

function init() {
    $("#Save").on("click", Save_click);
    $("#UpdateUpdate").on("click", Update_Click);
    $("#DeleteUpdate").on("click", Delete_Click);
    $("#DataClear").on("click", DataClear);

    $("#AWRecords").on("pageshow", AWShowAll_show);
    $("#AWEditFeedback").on("pageshow", AWEdit_show);
    $("#AWAddRecords").on("pageshow", showList);
}
function initDB(){
    try {
        DB.createDatabase();
        if (db) {
            console.info("Creating tables...");

            DB.AWcreateTablesRecord();

            DB.AWDropTablesFirst();

            DB.AWCreateTablesFirst();

            var South = "South America";
            var Europe = 'Europe'
            var Asia = "Asia";
            var Africa = 'Africa';
            var North = "North America";
            var options = [South];
            function callback() {
                console.info("Record inserted successfully");
            }
            DB.insert(options, callback);
            options = [Europe];
            DB.insert(options,callback())
            options = [Asia]
            DB.insert(options,callback());
            options = [Africa]
            DB.insert(options,callback());
            options = [North]
            DB.insert(options,callback());
        }
        else{
            console.error("Error: Cannot create Tables: database does not exist!");
        }
    } catch (e) {
        console.error("Error: (Fatal) Error in initDB(). Can not proceed");
    }
}
$(document).ready(function () {
    init();
    initDB();
});