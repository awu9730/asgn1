function checkFormAdd(){
    if(doValidate_AWAddBusiness()){
        //Form ok
        var userName = $("#AwName").val();
        var age = $("#AWAge").val();
        var email = $("#AwEmail").val();
        var description = $("#AwComment").val();
        var startDate = $("#AwDate").val();
        var typeId = "0";
        var watchOther = "false";
        var otherCount = "0";

        if($("#CreateType").val() == "South America") {
            typeId = parseInt("1");
        }
        else if($("#CreateType").val() == "Europe") {
            typeId = parseInt("2");
        }
        else if ($("#CreateType").val() == "Asia"){
            typeId = parseInt("3");
        }
        else if ($("#CreateType").val() == "Africa"){
            typeId = parseInt("4");
        }
        else if ($("#CreateType").val() == "North America"){
            typeId = parseInt("5");
        }

        if ($("#checkToggleCreate").prop("checked")){
            watchOther = ("true");
        }
        else{
            watchOther = ("false");
        }

        var options = [userName,typeId,age,email,description,startDate, watchOther, otherCount];

        function callback() {
            console.info("Record inserted successfully");
            alert("Feedback Added Successfully");
        }

        Review.insert(options,callback());

    }
    else{
        console.info("form is INVALID");
    }
}

function checkFormUpdate(){
    if(doValidate_AWEdit()){
        var id = localStorage.getItem("id");

        var name = $("#AWEditName").val();
        var age = $("#AWEditAge").val();
        var email = $("#AWEditMail").val();
        var comment = $("#AWEditComment").val();
        var date = $("#AWEditDate").val();
        var typeId = "0";
        var watchOther = "false";
        var otherCount = $("#EditTotal").val();

        if($("#ModifyType").val() == "South America") {
            typeId = parseInt("1");
        }
        else if($("#ModifyType").val() == "Europe") {
            typeId = parseInt("2");
        }
        else if ($("#ModifyType").val() == "Asia"){
            typeId = parseInt("3");
        }
        else if ($("#ModifyType").val() == "Africa"){
            typeId = parseInt("4");
        }
        else if ($("#ModifyType").val() == "North America"){
            typeId = parseInt("5");
        }

        if ($("#checkToggleEdit").prop("checked")){
            watchOther = ("true");
        }
        else
        {
            watchOther = ("false");
        }
        var options = [name,typeId,age, email, comment, date, watchOther,otherCount, id];

        function callback() {
            console.info("Record Update successfully");
            alert("Feedback Updated Successfully");
        }

        Review.update(options,callback());
    }
    else{
        console.info("form is INVALID");
    }
}
function deleteRecord(){
    var id = localStorage.getItem("id");

    var options = [id];
    function callback() {
        alert("Record deleted successfully");
        //todo move away from current page
        $(location).prop('href', '#AWRecords');

    }
    Review.delete(options, callback);
}

function clearDataBase(){
    var result = confirm("Really want to clear database");
    if (result) {
        try{
            DB.AWDropTablesRecord();
            alert("Database cleared");
        }catch (e){
            alert(e);
        }
    }
}

function showRecord(){
    var id = localStorage.getItem("id");

    var options = [id];

    function callback(tx, results) {
        var row = results.rows[0];
        //Select Everything
        console.info("Id: " + row['id'] +
            " userName: " + row['userName'] +
            " typeId: " + row['typeId'] +
            " age: " + row['age'] +
            " email: " + row['email'] +
            " description: " + row['description'] +
            " startDate: " + row['startDate'] +
            " watchOther: " + row['watchOther'] +
            " otherCount : " + row['otherCount']
        );
        //Name
        $("#AWEditName").val(row['userName']);
        //Select type
        if(row['typeId'] == '1') {
            $("#ModifyType").val("South America");
        }
        else if (row['typeId'] == '2'){
            $("#ModifyType").val("Europe");
        }
        else if (row['typeId'] == '3') {
            $("#ModifyType").val("Asia");
        }
        else if (row['typeId'] == '4'){
            $("#ModifyType").val("Africa");
        }
        else{
            $("#ModifyType").val("North America");
        }
        $('#ModifyType').selectmenu('refresh');
        //Values
        $("#AWEditAge").val((row['age']))
        $("#AWEditMail").val(row['email']);
        $("#AWEditDate").val(row['startDate']);
        $("#AWEditComment").val(row['description']);
        $("#EditQuality").val(row['otherCount']);

        if (row['watchOther'] == 'true') {
            $("#checkToggleEdit").prop("checked", true).checkboxradio('refresh');
            $(document).ready(function(){
              $("#section1").show();
            });
        }
        else{
            $("#checkToggleEdit").prop("checked", false).checkboxradio('refresh');
            $(document).ready(function(){
                $("#section1").hide();
            });
        }
    }
    Review.select(options, callback);
    $("#ModifyType").select("refresh");
}

function showRecordAll(){
    $("#lvAll").html("Empty");
    $("#lvAll").listview("refresh");

    var options = [];

    function callback(tx, results) {
        var htmlCode = "";
        for (var i = 0; i < results.rows.length; i++) {
            var row = results.rows[i];
            console.info("Id: " + row['id'] +
                " userName: " + row['userName'] +
                " typeId: " + row['typeId'] +
                " age: " + row['age'] +
                " email: " + row['email'] +
                " description: " + row['description'] +
                " startDate: " + row['startDate'] +
                " watchOther: " + row['watchOther'] +
                " otherCount : " + row['otherCount']
            );

            htmlCode += "<li><a data-role='button' data-row-id=" + row['id'] + " href='#'>" +
                "<h1>Yukimin Name: " + row['userName'] + "</h1>" +
                "<h2>Email: " + row['email'] + "</h2>" +
                "<h3>StartDate: " + row['startDate'] + "</h3>" +
                "</a></li>";
        }

        var lv = $("#lvAll");
        lv = lv.html(htmlCode);
        lv.listview("refresh"); // very important

        function clickHandler() {
            localStorage.setItem("id", $(this).attr("data-row-id"));
            $(location).prop('href', '#AWEditFeedback');
        }

        $("#lvAll a").on("click", clickHandler);
    }

    Review.selectAll(options, callback);
}

function showContinent(){
    $("#CreateType").html("Empty");
    $("#CreateType").select("refresh");

    var options = [];

    function callback(tx, results) {
        var htmlCode = "";
        for (var i = 0; i < results.rows.length; i++) {
            var row = results.rows[i];
            console.info("Id: " + row['id'] +
                " Name: " + row['name']
            );

            htmlCode += "<option>" + row['name'] + "</option>"
        }

        var lv = $("#CreateType");
        lv = lv.html(htmlCode);
        lv.select("refresh"); // very important
    }
    DB.AWSelectContinentAll(options,callback);
    $('#CreateType').selectmenu('refresh');
}

function continentList(){
    $("#ModifyType").html("Empty");
    $("#ModifyType").select("refresh");

    var options = [];

    function callback(tx, results) {
        var htmlCode = "";
        for (var i = 0; i < results.rows.length; i++) {
            var row = results.rows[i];
            console.info("Id: " + row['id'] +
                " Name: " + row['name']
            );

            htmlCode += "<option>" + row['name'] + "</option>"
        }

        var lv = $("#ModifyType");
        lv = lv.html(htmlCode);
        lv.select("refresh"); // very important
    }
    DB.AWSelectContinentAll(options,callback);
}