//global database object
var db;

//general purpose error handler
function errorHandler(error) {
    console.error("SQL error:     " + error.message);
}

var DB = {
    createDatabase: function () {
        var shortName = "AWFinalDB";
        var version = "1.0";
        var displayName = "DB for The final project";
        var dbSize = 2 * 1024 * 1024;

        function dbCreateSuccess() {
            console.info("Success: Database created successfully");
        }

        db = openDatabase(shortName, version, displayName, dbSize, dbCreateSuccess);
    },
    AWCreateTablesFirst: function () {
        function txFunction(tx) {
            var sql = "CREATE TABLE IF NOT EXISTS continent( "

                + "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"

                + "name VARCHAR(20) NOT NULL);";
            var options = [];


            function successCallback() {
                console.info("Success: Creating tables successful");
            }

            // step 2
            tx.executeSql(sql, options, successCallback, errorHandler);
        }

        function successTransaction() {
            console.info("Success: Create table transaction is successful");
        }

        //step 1
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    AWcreateTablesRecord: function () {
        function txFunction(tx) {
            var sql ="CREATE TABLE IF NOT EXISTS records( " +

                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +

                "userName VARCHAR(30) NOT NULL," +

                "typeId INTEGER NOT NULL," +

                "age INTEGER," +

                "email VARCHAR(30)," +

                "description TEXT," +

                "startDate DATE," +

                "watchOther VARCHAR(1)," +

                "otherCount INTEGER," +

                "FOREIGN KEY(typeId) REFERENCES continents(id));";

            var options = [];

            function successCallback() {
                console.info("Success: Creating tables successful");
            }

            // step 2
            tx.executeSql(sql, options, successCallback, errorHandler);
        }

        function successTransaction() {
            console.info("Success: Create table transaction is successful");
        }

        //step 1
        db.transaction(txFunction, errorHandler, successTransaction);

    },
    AWDropTablesFirst: function () {
        function txFunction(tx) {
            var sql = "DROP TABLE IF EXISTS continent;";
            var options = [];


            function successCallback() {
                console.info("Success: Dropping tables successful");
            }

            // step 2
            tx.executeSql(sql, options, successCallback, errorHandler);
        }

        function successTransaction() {
            console.info("Success: Dropping table transaction is successful");
        }

        //step 1
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    insert: function(options, callback){
        function txFunction(tx) {
            var sql = "INSERT INTO continent(name) VALUES(?);";
            tx.executeSql(sql, options, callback, errorHandler );
        }
        function successTransaction() {
            console.info("Success: Insert transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    AWDropTablesRecord: function () {
        function txFunction(tx) {
            var sql = "DROP TABLE IF EXISTS records;";
            var options = [];


            function successCallback() {
                console.info("Success: Dropping tables successful");
            }

            // step 2
            tx.executeSql(sql, options, successCallback, errorHandler);
        }

        function successTransaction() {
            console.info("Success: Dropping table transaction is successful");
        }

        //step 1
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    AWSelectContinentAll: function (options, callback){
        function txFunction(tx) {
            var sql = "SELECT * FROM continent;";
            tx.executeSql(sql, options, callback, errorHandler );
        }
        function successTransaction() {
            console.info("Success: Insert transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    }
}
