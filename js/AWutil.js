function doValidate_AWAddBusiness(){
    // 1. Retrieve the ref to the form
    var f = $("#AWAddRecord");
    // 2. Call the validate() function on the form object and pass validation rules as an object
    f.validate({
        rules: {
            AwName:{
                required: true,
                minlength: 2
            },
            AWAge:{
                required: true
            },
            AwEmail:{
                required: true,
                email: true,
            },

            AwDate:{
                required: true
            },
            createTotal: {
                required: true,
                range: [0, 51]
            }
        },
        messages: {
            AwName:{
                required: "Name is required, do you not have a name?",
                minlength: "Length must be longer than 2"
            },
            AWAge:{
                required: "Age is Required, did you forgot your birthday?"
            },

            AwEmail:{
                required: "Email is required",
                email: "email must be valid" ,
            },
            AwDate:{
                required: "Date is required"
            },
            createTotal: {
                required: 'you need a number here, did you accidentally deleted the number?',
                range : 'There is only 51 other members in Hololive excluding Lamy!'
            }
        }
    });
    // 3. return valid() on the form
    return f.valid();
}
function doValidate_AWEdit(){
    // 1. Retrieve the ref to the form
    var f = $("#AWEdit");
    // 2. Call the validate() function on the form object and pass validation rules as an object
    f.validate({
        rules: {
            AWEditName:{
                required: true,
                minlength: 2
            },
            AWEditAge:{
                required: true
            },
            AWEditMail:{
                required: true,
                email: true,
            },
            AWEditDate:{
                required: true
            },
            EditTotal: {
                required: true
            }
        },
        messages: {
            AwEditName:{
                required: "Name is required",
                minlength: "Length must be longer than 2"
            },
            AwEditMail:{
                required: "Email is required",
                email: "email must be valid" ,
            },
            AWEditAge:{
                required: "You're not un-aged are you?"
            },
            AwEditDate:{
                required: "Date is required"
            },
            EditTotal:{
                required: "There is only 51 other members in Hololive excluding Lamy!"
            }
        }
    });
    // 3. return valid() on the form
    return f.valid();
}
