var Review = {
    insert: function(options, callback){
        function txFunction(tx) {
            var sql = "INSERT INTO records(userName, typeId, age, email, description, startDate, watchOther, otherCount) VALUES(?,?,?,?,?,?,?,?);";
            tx.executeSql(sql, options, callback, errorHandler );
        }
        function successTransaction() {
            console.info("Success: Insert transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    update: function(options, callback){
        function txFunction(tx) {
            var sql = "UPDATE records SET userName=?, typeId=?, age=?, email=?, description=?, startDate=?, watchOther=?, otherCount=? WHERE id=?;";
            tx.executeSql(sql, options, callback, errorHandler );
        }
        function successTransaction() {
            console.info("Success: Update transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    delete: function(options, callback){
        function txFunction(tx) {
            var sql = "DELETE FROM records WHERE id=?;";
            tx.executeSql(sql, options, callback, errorHandler );
        }
        function successTransaction() {
            console.info("Success: Delete transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    select: function(options, callback){
        function txFunction(tx) {
            var sql = "SELECT * FROM records WHERE id=?;";
            tx.executeSql(sql, options, callback, errorHandler );
        }
        function successTransaction() {
            console.info("Success: Select transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    selectAll: function(options, callback){
        function txFunction(tx) {
            var sql = "SELECT * FROM records;";
            tx.executeSql(sql, options, callback, errorHandler );
        }
        function successTransaction() {
            console.info("Success: Insert transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    }
};